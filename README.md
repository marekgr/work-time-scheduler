

# Work Time Scheduler

This project was generated using [Nx](https://nx.dev).

## Aim

Idea for the project came when I couldn't find App suitable to my needs and decided to code it myself.

The goal of this project was to learn on how to create applications for mobile and desktop environment, while sharing common code in monorepo.


## Summary

This projects consists of two applications designed for use in mobile and desktop environment.

Web part is being developed using Angular framework, and mobile version uses Ionic in its core, while entire logic is written using Angular framework aswell.

Backend services of both apps will be provided by Firebase for entirety of its functions, such as storing data, authorization of users and handling sessions.

## Build and run

Required in order to run app locally:
- `node.js` in version `14.15.5` or newer
- Android Studio (for packaging mobile app and running it on the phone)


To run locally in browser:
- `npm install` in main directory
- `ng serve ionic-mobile` to run mobile app in browser
- `ng serve web-app` to run web application in browser
Both of the apps when run, will be hosted locally on `localhost:4200`

To package mobile application and run it on Android device:
- make sure you have Android Studio installed on your computer
- build ionic/angular application: `nx build ionic-mobile`
- synchronize mobile build with android envirionment: `nx run ionic-mobile:sync:android`
- run android environment for mobile app: `nx run ionic-mobile:open:android`. This command should open Android Studio.

In Android Studio IDE you can either run the app in emulator using Run Configurations, or package app to APK file via:
`Build -> Build Bundle(s) / APK(s) -> Build APK(s)`
After build is complete, AS will display notification in the bottom right corner when you can click `locate` link to open APK file location. Then just move file to your phone and install it.
