import firebase from 'firebase/compat';
import { v4 as uuidv4 } from 'uuid';
import Timestamp = firebase.firestore.Timestamp;

export class MonthSchedule {
  id: string = uuidv4();
  month: number;
  year: number;
  schedules: DayScheduleItem[] = [];

  constructor(month: number, year: number) {
    this.month = month;
    this.year = year;
  }
}

export class DayScheduleItem {
  id: string = uuidv4();
  date: Date = new Date();
  timeFrom: string = '07:00';
  timeTo: string = '15:00';
  timeDiff: number = 8;
  description?: string;
}

export interface DayScheduleItemDto {
  id: string,
  date: Timestamp,
  timeFrom: string;
  timeTo: string;
  timeDiff: number;
  description?: string;
}

export interface MontScheduleModelDto {
  id: string;
  userId: string;
  month: number;
  year: number;
  schedules: DayScheduleItemDto[];
}
