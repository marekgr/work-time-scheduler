# Repo creation command. Selected angular template
npx create-nx-workspace@12.10.1

# cd inside project
cd work-time-scheduler

# Install @nxtend/ionic-angular and @nxtend/capacitor
npm install --save-dev @nxtend/ionic-angular
npm install --save-dev @nxtend/capacitor

# Add ionic app to monorepo using nxtend script
nx generate @nxtend/ionic-angular:application ionic-mobile

#Setup capacitor to work with ionic app
cd apps
nx generate @nxtend/capacitor:capacitor-project --project ionic-mobile

# Setup android platform
nx run ionic-mobile:add:android

# Build ionic
nx build ionic-mobile

# Sync mobile build with android native
nx run ionic-mobile:sync:android

# Open platform in Android Studio
nx run ionic-mobile:open:android
