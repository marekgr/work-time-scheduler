import { Component } from '@angular/core';

@Component({
  selector: 'work-time-scheduler-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'web-app';
}
