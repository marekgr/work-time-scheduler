import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.grzejszczak.app',
  appName: 'Work time scheduler',
  webDir: '../../dist/apps/ionic-mobile',
  bundledWebRuntime: false
};

export default config;
