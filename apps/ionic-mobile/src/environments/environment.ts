// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCPz6Jf4rZXZyBtHF8uSbBoNxgKfV95LQI",
    authDomain: "work-time-schedule.firebaseapp.com",
    projectId: "work-time-schedule",
    storageBucket: "work-time-schedule.appspot.com",
    messagingSenderId: "598291724741",
    appId: "1:598291724741:web:d049cefa64f0d338b1f3ac"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
