import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/authentication/login/login.component';
import { RegisterComponent } from './components/authentication/register/register.component';
import { MainComponent } from './components/main/main.component';
import { AngularFireAuthGuard } from '@angular/fire/compat/auth-guard';
import { redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { ExportComponent } from './components/export/export.component';
import { AboutComponent } from './components/about/about.component';
import { SettingsComponent } from './components/settings/settings.component';
import {
  RegisterConfirmEmailComponent
} from "./components/authentication/register/register-confirm-email/register-confirm-email.component";
import {
  LoginForgotPasswordComponent
} from "./components/authentication/login/login-forgot-password/login-forgot-password.component";

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['auth/login']);

const routes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: MainComponent, canActivate: [AngularFireAuthGuard],  data: { authGuardPipe: redirectUnauthorizedToLogin } },
  { path: 'export', component: ExportComponent, canActivate: [AngularFireAuthGuard],  data: { authGuardPipe: redirectUnauthorizedToLogin } },
  { path: 'about', component: AboutComponent, canActivate: [AngularFireAuthGuard],  data: { authGuardPipe: redirectUnauthorizedToLogin } },
  { path: 'settings', component: SettingsComponent, canActivate: [AngularFireAuthGuard],  data: { authGuardPipe: redirectUnauthorizedToLogin } },
  { path: 'auth/login', component: LoginComponent, },
  { path: 'auth/login/forgot-password', component: LoginForgotPasswordComponent, },
  { path: 'auth/register', component: RegisterComponent, },
  { path: 'auth/register/confirm', component: RegisterConfirmEmailComponent, },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
