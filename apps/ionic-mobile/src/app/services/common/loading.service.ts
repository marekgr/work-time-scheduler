import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  loader: HTMLIonLoadingElement;

  constructor(private loadingController: LoadingController, private translate: TranslateService) { }

  presentLoading(translationKey: string) {
    const message = this.translate.instant(`${translationKey}`);
     this.loadingController.create({
      message: message,
    }).then(it => {
       this.loader = it;
       this.loader.present();
     });
  }

  presentFixedLoading(translationKey: string, duration: number) {
   this.presentLoading(translationKey);
   setTimeout(() => this.dismissLoading(), duration)
  }

  dismissLoading() {
    this.loader.dismiss();
  }
}
