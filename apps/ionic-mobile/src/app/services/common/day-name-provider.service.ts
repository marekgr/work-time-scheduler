import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class DayNameProviderService {

  weekDays = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];

  constructor(private translate: TranslateService) { }

  getShortDayName(dayOfTheWeek: number) {
    return this.translate.instant(`calendar.weekDays.${this.weekDays[dayOfTheWeek]}`);
  }

  getFullDayName(dayOfTheWeek: number) {
    return this.translate.instant(`calendar.weekDaysFull.${this.weekDays[dayOfTheWeek]}`);
  }

  getDayWithMonthShort(date: Date) {
    return `${date.getDate()} ${date.toLocaleString(this.translate.currentLang, { month: 'short' })}.`;
  }
}
