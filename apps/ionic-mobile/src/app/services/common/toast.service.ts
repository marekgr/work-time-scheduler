import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastController: ToastController) { }

  presentShort(message: string) {
    this.toastController.create(
      {
        message: message,
        duration: 2000
      }
    ).then(it => it.present())
  }
}
