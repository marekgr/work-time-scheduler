import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/compat/auth';
import {Router} from '@angular/router';
import {AngularFirestore} from '@angular/fire/compat/firestore';
import {UpdateUserDataRequest, UserConfig, UserService} from './user.service';
import firebase from 'firebase/compat';
import {ToastService} from "./common/toast.service";
import {TranslateService} from "@ngx-translate/core";
import {BehaviorSubject, Observable} from "rxjs";
import { LoadingService } from './common/loading.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  tempUser: firebase.User | null;

  private userLogged$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private auth: AngularFireAuth,
              private fireStore: AngularFirestore,
              private router: Router,
              private userService: UserService,
              private toast: ToastService,
              private translate: TranslateService,
              private loading: LoadingService) {
  }

  setUserLogged(state: boolean, user: firebase.User | null = null): void {
    this.tempUser = user;
    this.userLogged$.next(state);
  }

  getUser(): Promise<firebase.User | null> {
    return this.auth.currentUser;
  }

  onUserStateChanged(): Observable<boolean> {
    return this.userLogged$;
  }

  async emailRegister(credentials: Credentials) {
    await this.auth.createUserWithEmailAndPassword(credentials.email, credentials.password).then((result) => {
      this.setUserLogged(true, result.user);
      if (this.tempUser != null) {
        const request = new UpdateUserDataRequest(this.tempUser.uid, this.tempUser.email, this.tempUser.displayName, new UserConfig())
        this.userService.updateUserData(request);
      }
      return result.user?.sendEmailVerification();
    })
  }

  async resendVerificationEmail() {
    this.loading.presentFixedLoading(this.translate.instant("common.wait"), 1500);
    await this.tempUser?.sendEmailVerification().finally(() => this.loading.dismissLoading());
  }

  async emailLogin(email: string, password: string) {
    return await this.auth.signInWithEmailAndPassword(email, password);
  }

  async logOut() {
    await this.auth.signOut();
    this.setUserLogged(false);
    await this.toast.presentShort(this.translate.instant('auth.login.successLogout'));
    return this.router.navigateByUrl('/auth/login', {replaceUrl: true});
  }

  async resetPassword(email: string): Promise<void> {
    return this.auth.sendPasswordResetEmail(email);
  }
}

export class Credentials {
  email = '';
  password = '';
  displayName = '';
}
