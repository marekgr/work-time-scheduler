import { Injectable } from '@angular/core';
import { DayScheduleItem } from '../../../../../libs/model/api';

@Injectable({
  providedIn: 'root'
})
export class SelectionService {

  selectedScheduleList: DayScheduleItem[] = [];

  constructor() {
  }

  isAnyItemSelected(): boolean {
    return this.selectedScheduleList.length > 0;
  }

  itemSelected(schedule: DayScheduleItem): boolean {
    return !!this.selectedScheduleList.find(it => it.id === schedule.id);
  }

  selectionChange(schedule: DayScheduleItem, selected: boolean): void {
    selected ? this.selectedScheduleList.push(schedule) : this.selectedScheduleList = this.selectedScheduleList.filter(item => item.id !== schedule.id);
  }
}
