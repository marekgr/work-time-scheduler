import { Injectable } from '@angular/core';
import { noop, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  DayScheduleItem,
  DayScheduleItemDto,
  MonthSchedule,
  MontScheduleModelDto
} from '../../../../../libs/model/api';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import firebase from 'firebase/compat/app';
import { AuthService } from './auth.service';
import Timestamp = firebase.firestore.Timestamp;

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  currentMonthSnapshot: MonthSchedule | null;

  cachedMonthSnapshots: Map<string, MonthSchedule> = new Map<string, MonthSchedule>();

  dateUpdated: Date;

  constructor(private auth: AuthService,
              private db: AngularFirestore) {
  }

  fetchMonth(request: FetchMontRequest): Observable<MonthSchedule> {
    const key = getYearMonthKey(request.query.year, request.query.month);
    const dataRef = this.db.doc(`userCreatedEvents/${request.userId}/scheduleData/${key}`);
    return dataRef.snapshotChanges()
      .pipe(
        map(changes => {
          const data = changes.payload.data() as MontScheduleModelDto;
          const result = data ? this.parseToMonthSchedule(data) : new MonthSchedule(request.query.month, request.query.year);
          this.currentMonthSnapshot = result;
          this.dateUpdated = new Date();
          return result;
        }),
      );
  }

  updateMonth(request: UpdateMonthRequest) {
    const key = getYearMonthKey(request.monthScheduleData.year, request.monthScheduleData.month);
    const dataRef = this.db.doc<MonthSchedule>(`userCreatedEvents/${request.userId}/scheduleData/${key}`);
    dataRef.set(this.parseToMonthScheduleDto(request.monthScheduleData), { merge: true }).then(noop);
  }

  clearLocalData(): void {
    this.currentMonthSnapshot = null;
    this.cachedMonthSnapshots = new Map();
  }

  private parseToMonthScheduleDto(data: MonthSchedule) {
    const dataInput = JSON.parse(JSON.stringify(data))
    dataInput.schedules.forEach((it: any) => it.date = Timestamp.fromDate(new Date(it.date)))
    return dataInput;
  }

  private parseToScheduleDayItem(input: DayScheduleItemDto): DayScheduleItem {
    return {
      id: input.id,
      date: input.date.toDate(),
      timeFrom: input.timeFrom,
      timeTo: input.timeTo,
      timeDiff: input.timeDiff,
      description: input.description,
    } as DayScheduleItem;
  }

  private parseToMonthSchedule(input: MontScheduleModelDto): MonthSchedule {
    return {
      id: input.id,
      month: input.month,
      year: input.year,
      schedules: input.schedules.map(it => this.parseToScheduleDayItem(it))
    } as MonthSchedule;
  }
}

export function getYearMonthKey(year: number, month: number): string {
  return `${year}_${month}`
}

export class FetchMontRequest {
  constructor(public userId: string, public query: MonthYear) {}
}

export class UpdateMonthRequest {
  constructor(public userId: string, public monthScheduleData: MonthSchedule) {}
}

export class MonthYear {
  constructor(public month: number, public year: number) {}
}
