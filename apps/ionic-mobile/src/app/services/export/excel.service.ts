import { Injectable } from '@angular/core';
import XlsxTemplate from 'xlsx-template-browserify';
import { HttpClient } from '@angular/common/http';
import { DateRange } from '@angular/material/datepicker';
import { combineLatest, firstValueFrom, lastValueFrom } from 'rxjs';
import { MonthYear, ScheduleService } from '../schedule.service';
import { DayScheduleItem, MonthSchedule } from '../../../../../../libs/model/api';
import { DayNameProviderService } from '../common/day-name-provider.service';
import { FileManagerService, FileType } from './file-manager.service';
import { ExportFormat } from '../../components/export/export.service';
import { LoadingService } from '../common/loading.service';
import { ToastService } from '../common/toast.service';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor(private http: HttpClient,
              private scheduleService: ScheduleService,
              private dayNameProvider: DayNameProviderService,
              private fileManager: FileManagerService,
              private loading: LoadingService,
              private auth: AuthService,
              private toast: ToastService) {
  }

  async generateXlsx(dateRange: DateRange<Date>, fileName: string) {
    this.loading.presentLoading('exportPage.exportInProgress');
    try {
      const user = await this.auth.getUser();
      if (!user) {
        return;
      }
      const template = await this.fetchXlsxTemplate();
      const exportData = await this.fetchExportData(dateRange, user.uid);

      const xlsxTemplate = new XlsxTemplate(this.toBuffer(template));
      const data = this.parseMonthDataToExcel(exportData, dateRange, user.displayName);
      xlsxTemplate.substitute(1, data);
      const result = xlsxTemplate.generate({type: 'arraybuffer'});
      const blob = new Blob([result], {type: FileType.xlsx});

      const file = await this.fileManager.writeToFile(blob, ExportFormat.xlsx, fileName);
      await this.fileManager.openFile(file, ExportFormat.xlsx);
      this.loading.dismissLoading()
    } catch (e) {
      this.handleError(e);
    }
  }

  private handleError(e: string) {
    this.loading.dismissLoading();
    this.toast.presentShort(e)
  }

  private parseMonthDataToExcel(monthSchedules: MonthSchedule[], dateRange: DateRange<Date>, userName: string | null): ExcelDataModel {
    let timeDiffSum = 0;
    const combinedScheduleList: DayScheduleItem[] = [];
    monthSchedules.map(it => {
      combinedScheduleList.push(...it.schedules);
      it.schedules.forEach(schedule => {
        timeDiffSum += schedule.timeDiff
      })
    });

    const scheduleRows = this.getExcelTableRows(combinedScheduleList, dateRange);
    return {
      userName: userName != null ? userName : '-',
      dateRange: `${dateRange.start?.toLocaleDateString()} - ${dateRange.end?.toLocaleDateString()}`,
      schedules: scheduleRows,
      timeDiffSum: `${Math.round(timeDiffSum * 100) / 100}h`,
    } as ExcelDataModel;
  }

  private getSchedulesForThatDay(combinedScheduleList: DayScheduleItem[], date: Date) {
    return combinedScheduleList.filter(it =>
      it.date.getDate() == date.getDate()
      && it.date.getMonth() == date.getMonth()
      && it.date.getUTCFullYear() == date.getUTCFullYear())
      .sort((a, b) => (a.timeFrom > b.timeFrom) ? 1 : ((b.timeFrom > a.timeFrom) ? -1 : 0));
  }

  private getExcelTableRows(combinedScheduleList: DayScheduleItem[], dateRange: DateRange<Date>): ExcelTableRow[] {
    const dates = this.getDatesBetween(dateRange.start, dateRange.end);
    const scheduleRows: ExcelTableRow[] = [];
    dates.forEach(date => {
      const schedulesForThatDay = this.getSchedulesForThatDay(combinedScheduleList, date)
      if (schedulesForThatDay.length < 1) {
        scheduleRows.push(this.getEmptyRow(date));
        return;
      }

      schedulesForThatDay.map((item, index) => {
        const row = {
          startTime: item.timeFrom,
          endTime: item.timeTo,
          timeDiff: `${Math.round(item.timeDiff * 100) / 100}h`
        } as ExcelTableRow;

        if (index < 1) {
          row.dayName = this.dayNameProvider.getFullDayName(date.getDay());
          row.dayNumber = this.dayNameProvider.getDayWithMonthShort(date);
        }
        scheduleRows.push(row)
      });
    })
    return scheduleRows;
  }

  private getEmptyRow(date: Date): ExcelTableRow {
    return {
      dayName: this.dayNameProvider.getFullDayName(date.getDay()),
      dayNumber: this.dayNameProvider.getDayWithMonthShort(date),
      startTime: '-',
      endTime: '-',
      timeDiff: '0h',
    } as ExcelTableRow
  }

  private fetchXlsxTemplate(): Promise<ArrayBuffer> {
    return lastValueFrom(this.http.get('assets/exportTemplates/xlsx/Template_1.xlsx', {responseType: 'arraybuffer'}));
  }

  private fetchExportData(dateRange: DateRange<Date>, userId: string): Promise<MonthSchedule[]> {
    const ranges: MonthYear[] = this.getMonthRanges(dateRange.start, dateRange.end);
    return firstValueFrom<MonthSchedule[]>(combineLatest(ranges.map(range => this.scheduleService.fetchMonth({
      query: range,
      userId: userId
    }))))
  }

  private getMonthRanges(startDate: any, endTate: any): MonthYear[] {
    const monthRanges: MonthYear[] = [];
    for (const dt = new Date(startDate); dt <= endTate; dt.setDate(dt.getDate() + 1)) {
      const range = new MonthYear(dt.getMonth(), dt.getUTCFullYear());
      if (!monthRanges.some(it => it.month === range.month && it.year === range.year)) {
        monthRanges.push(range);
      }
    }
    return monthRanges;
  };

  private getDatesBetween(startDate: any, endDate: any): Date[] {
    const dates: Date[] = [];
    for (let dt = new Date(startDate); dt <= endDate; dt.setDate(dt.getDate() + 1)) {
      dates.push(new Date(dt));
    }
    return dates;
  }

  private toBuffer(ab: ArrayBuffer): Buffer {
    const buf = Buffer.alloc(ab.byteLength);
    const view = new Uint8Array(ab);
    for (let i = 0; i < buf.length; ++i) {
      buf[i] = view[i];
    }
    return buf;
  }
}

export class ExcelDataModel {
  userName: string;
  dateRange: string;
  schedules: ExcelTableRow[];
  timeDiffSum: string;
}

export class ExcelTableRow {
  dayName = '';
  dayNumber: string;
  startTime = '';
  endTime = '';
  timeDiff: string;
}
