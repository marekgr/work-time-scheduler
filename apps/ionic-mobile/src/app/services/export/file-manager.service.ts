import { Injectable } from '@angular/core';
import { Directory, Filesystem } from '@capacitor/filesystem';
import * as moment from 'moment';
import { ExportFormat } from '../../components/export/export.service';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';

@Injectable({
  providedIn: 'root'
})
export class FileManagerService {

  constructor(private fileOpener: FileOpener) {
  }

  async writeToFile(data: Blob, format: ExportFormat, fileName: string) {
    const file = `${fileName}.${format}`;
    const blobData = await this.blobToBase64(data);

    return this.writeData(file, blobData);
  }

  async openFile(fileName: string, format: ExportFormat) {
    let fileType = ''
    switch (format) {
      case ExportFormat.xlsx: fileType = FileType.xlsx
    }
    const filePath = await Filesystem.getUri({path: fileName, directory: Directory.Documents})
    await this.fileOpener.open(filePath.uri, fileType)
  }

  private async writeData(fileName: string, data: string) {
    await Filesystem.writeFile({
      path: `${fileName}`,
      data: data,
      directory: Directory.Documents
    });
    return fileName;
  }

  blobToBase64: (blob: Blob) => Promise<string> = (blob: Blob) => {
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    return new Promise(resolve => {
      reader.onloadend = () => {
        resolve(reader.result as string);
      };
    });
  };
}

export enum FileType{
  xlsx = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
}
