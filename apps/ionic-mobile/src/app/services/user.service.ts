import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  config: UserConfig = new UserConfig();

  configChanged: Subject<UserConfig> = new Subject<UserConfig>();

  appVersion: 1.01;

  constructor(private fireStore: AngularFirestore) {}

  setConfig(config: UserConfig): void {
    this.config = JSON.parse(JSON.stringify(config));
    this.configChanged.next(this.config);
  }

  getConfig(): UserConfig  {
    return JSON.parse(JSON.stringify(this.config));
  }

  updateUserData(request: UpdateUserDataRequest): Promise<void> {
    const userRef: AngularFirestoreDocument<UserData> = this.fireStore.doc(`users/${request.uid}`);
    const data = JSON.parse(JSON.stringify(request)) as UserData;
    return userRef.set(data, { merge: true }).then(() => this.setConfig(request.config))
  }

  fetchUserConfig(request: FetchUserDataRequest): Observable<UserConfig> {
    const dataRef = this.fireStore.doc<UserData>(`users/${request.uid}`);
    return dataRef.snapshotChanges()
      .pipe(
        map(changes => {
          return changes.payload.data()?.config as UserConfig;
        }),
      );
  }
}

export class UpdateUserDataRequest {
  constructor(public uid: string,
              public email: string | null,
              public displayName: string | null,
              public config: UserConfig) {}
}

export class FetchUserDataRequest {
  constructor(public uid: string) {
  }
}

export class UserData {
  uid: string;
  email: string | null;
  displayName: string | null;
  config: UserConfig;
}

export class UserConfig {
  general: GeneralSettings = new GeneralSettings();
  calendar: CalendarSettings = new CalendarSettings();
  schedules: ScheduleSettings = new ScheduleSettings();
}

export class GeneralSettings {
  language = 'pl';
  theme = 'default';
}

export class CalendarSettings {
  timezone: string;
  timeFormat: '12h' | '24h' = '24h';
  weekStartDay: 'sun' | 'mon' = 'mon';
  showFulfilledDates = true;
}

export class ScheduleSettings {
  defaultWorkStartTime = '07:00';
  defaultWorkEndTime = '15:00';
  defaultDescription: string;
}
