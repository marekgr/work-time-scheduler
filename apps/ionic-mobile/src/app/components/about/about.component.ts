import { Component, OnInit } from '@angular/core';
import { ScheduleService } from '../../services/schedule.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'wts-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(public scheduleService: ScheduleService,
              public dataService: UserService) { }

  ngOnInit(): void {
  }

}
