import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { AuthService, Credentials } from '../../../services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import firebase from 'firebase/compat';
import { LoadingService } from '../../../services/common/loading.service';
import FirebaseError = firebase.FirebaseError;


export class RepeatPasswordEStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    // @ts-ignore
    return (control && control.parent.get('password').value !== control.parent.get('confirmPassword').value && control.dirty)
  }
}

@Component({
  selector: 'wts-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent {

  hide = true;

  registerForm: FormGroup;

  matcher = new RepeatPasswordEStateMatcher();

  accountExists: boolean;

  @ViewChild('successfulRegister')
  registerSuccessDialog: any;

  get email(): AbstractControl {
    return this.registerForm.controls.email;
  }

  get password(): AbstractControl {
    return this.registerForm.controls.password;
  }

  get confirmPassword(): AbstractControl {
    return this.registerForm.controls.confirmPassword;
  }

  constructor(public auth: AuthService,
              private translate: TranslateService,
              private loading: LoadingService,
              private toastController: ToastController,
              private fb: FormBuilder,
              private router: Router) {
    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      displayName: [''],
      password: ['', [Validators.minLength(5), Validators.required, Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9#?!@$%^&*-_]+$')]],
      confirmPassword: [''],
    }, { validators: this.checkPasswords })
  }

  async register() {
    const userData = this.registerForm.getRawValue() as Credentials;
    this.loading.presentLoading('auth.register.registrationInProgress');
    this.auth.emailRegister(userData)
      .then(() => this.router.navigate(['/auth/register/confirm']))
      .catch(error => this.handleError(error))
      .finally(() => this.loading.dismissLoading());
  }

  async handleError(error: FirebaseError) {
    if (error.code === 'auth/email-already-in-use') {
      this.accountExists = true;
      return;
    }

    const toast = await this.toastController.create({
      message: error.message,
      duration: 2000
    });
    await toast.present();
  }

  private checkPasswords(group: FormGroup) {
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;
    return pass === confirmPass ? null : { notSame: true };
  }
}
