import {Component} from '@angular/core';
import {AuthService} from "../../../../services/auth.service";

@Component({
  selector: 'wts-register-confirm-email',
  templateUrl: './register-confirm-email.component.html',
  styleUrls: ['./register-confirm-email.component.scss']
})
export class RegisterConfirmEmailComponent {

  constructor(public auth: AuthService) { }
}
