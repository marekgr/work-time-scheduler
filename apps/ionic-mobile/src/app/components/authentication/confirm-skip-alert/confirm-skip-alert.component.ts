import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'wts-confirm-skip-alert',
  templateUrl: './confirm-skip-alert.component.html',
  styleUrls: ['./confirm-skip-alert.component.scss']
})
export class ConfirmSkipAlertComponent implements OnInit {

  constructor(public alertController: AlertController,
              private translate: TranslateService) { }

  ngOnInit(): void {
  }

  async presentAlert() {
    await this.translate.get('auth.confirmSkip').subscribe(async translations => {
      const alert = await this.alertController.create({
        header: translations.header,
        message: translations.message,
        buttons: [
          translations.cancel,
          {
            text: translations.cancel,
            handler: () => {
              console.log('Agree')
            }
          }]
      });
      await alert.present();
    })
  }
}
