import { Component } from '@angular/core';
import { AuthService } from "../../../../services/auth.service";
import { LoadingService } from "../../../../services/common/loading.service";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";

@Component({
  selector: 'wts-login-forgot-password',
  templateUrl: './login-forgot-password.component.html',
  styleUrls: ['./login-forgot-password.component.scss']
})
export class LoginForgotPasswordComponent {

  email: string;

  constructor(private auth: AuthService,
              private loadingService: LoadingService,
              private translation: TranslateService,
              private router: Router) { }

  sendResetPasswordEmail(): void {
    this.loadingService.presentLoading('Loading')
    this.auth.resetPassword(this.email).then(value => {
      this.loadingService.dismissLoading();
      this.router.navigateByUrl('auth/login');
    })
  }
}
