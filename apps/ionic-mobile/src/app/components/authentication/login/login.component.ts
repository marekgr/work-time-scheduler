import { Component } from '@angular/core';
import { AuthService, Credentials } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from '../../../services/common/toast.service';
import { LoadingService } from '../../../services/common/loading.service';

@Component({
  selector: 'wts-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  credentials = new Credentials();

  hide = true;

  incorrectCredentials = false;

  constructor(private auth: AuthService,
              private toast: ToastService,
              private loading: LoadingService,
              private translate: TranslateService,
              private router: Router) {
  }

  async loginEmail() {
    this.loading.presentLoading('auth.login.loginInProgress');
    this.auth.emailLogin(this.credentials.email, this.credentials.password).then(async value => {
      this.auth.setUserLogged(true, value.user);
      if(value.user?.emailVerified) {
        await this.toast.presentShort(this.translate.instant('auth.login.success'));
        await this.router.navigateByUrl('', { replaceUrl: true });
      } else {
        await this.router.navigate(['/auth/register/confirm']);
      }
    }).catch(async () => {
      this.incorrectCredentials = true;
      await this.toast.presentShort(this.translate.instant('auth.login.incorrectCredentials'))
    }).finally( async () => {
      await this.loading.dismissLoading();
    });
  }
}
