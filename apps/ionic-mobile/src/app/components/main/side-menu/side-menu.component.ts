import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { AuthService } from '../../../services/auth.service';
import { ScheduleService } from '../../../services/schedule.service';

@Component({
  selector: 'wts-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  constructor(private menuController: MenuController,
              public navCtrl: NavController,
              private scheduleService: ScheduleService,
              public auth: AuthService) { }

  ngOnInit(): void {
  }

  openMenu(): void {
    this.menuController.open('sideMenu')
  }

  async logOut() {
    await this.auth.logOut();
    this.scheduleService.clearLocalData();
    this.dismissMenu();

  }

  dismissMenu(): void {
    this.menuController.close('sideMenu');
  }

  navigateTo(page: string) {
    this.navCtrl.navigateForward([page], {animated: true});
    this.dismissMenu();
  }

}
