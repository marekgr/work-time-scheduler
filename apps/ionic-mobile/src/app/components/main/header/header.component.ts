import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'wts-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  today = ''

  constructor(private translate: TranslateService) {
    this.setTodayDateForLang(translate.currentLang)
    this.translate.onLangChange.subscribe(data => {
      this.setTodayDateForLang(data.lang)
    })
  }

  private setTodayDateForLang(language: string) {
    this.today = new Date().toLocaleDateString(language, {
      day: 'numeric',
      month: 'long',
      year: 'numeric'
    })
  }
}
