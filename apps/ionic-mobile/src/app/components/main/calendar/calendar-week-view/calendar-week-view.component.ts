import {
  Component,
  EventEmitter,
  Input,
  OnChanges, OnDestroy,
  Output,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import * as moment from 'moment';
import { UserService } from '../../../../services/user.service';
import { noop, Subscription } from 'rxjs';

@Component({
  selector: 'wts-calendar-week-view',
  templateUrl: './calendar-week-view.component.html',
  styleUrls: ['./calendar-week-view.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CalendarWeekViewComponent implements OnChanges, OnDestroy {

  SLIDE_OPTIONS = {
    loop: true,
    initialSlide: 1,
    runCallbacksOnInit: false,
    loopPreventsSlide: false,
    touchReleaseOnEdges: true,
    speed: 400
  };

  @Input()
  selectedDate: Date = new Date();

  @Input()
  fullFilledDates: Date[] = [];

  @Output()
  dateChange: EventEmitter<Date> = new EventEmitter<Date>();

  @ViewChild('ionSlides')
  ionSlides: HTMLIonSlidesElement;

  weekDays = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];

  previousWeek: Date[] = []
  currentWeek: Date[] = [];
  nextWeek: Date[] = [];

  currentlyViewedWeek = 0;
  today = new Date().toDateString();

  private subscription: Subscription;

  constructor(private userService: UserService) {
    this.updateWeekDates(this.selectedDate);
    this.subscription = this.userService.configChanged.subscribe(() => this.updateWeekDates(this.selectedDate));
  }

  ngOnChanges(changes: SimpleChanges) {
    const date: Date = changes.selectedDate?.currentValue;
    if (date) {
      this.currentlyViewedWeek = 0
      this.updateWeekDates(date);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  slideNext(): void {
    this.currentlyViewedWeek = this.currentlyViewedWeek + 1;
    this.updateWeekDates(this.selectedDate);
    this.ionSlides.slideTo(2, 0, false).then(noop);
  }

  slidePrevious(): void {
    this.currentlyViewedWeek = this.currentlyViewedWeek - 1;
    this.updateWeekDates(this.selectedDate);
    this.ionSlides.slideTo(2, 0, false).then(noop);
  }

  isToday(date: Date): boolean {
    return this.today === date.toDateString();
  }

  onDateSelected(date: Date): void {
    this.selectedDate = date;
    this.dateChange.emit(date);
  }

  isWeekend(date: Date): boolean {
    return date.getDay() === 0 || date.getDay() === 6;
  }

  private updateWeekDates(date: Date) {
    this.previousWeek = this.getDatesOfTheWeek(this.currentlyViewedWeek - 1, date);
    this.currentWeek = this.getDatesOfTheWeek(this.currentlyViewedWeek, date);
    this.nextWeek = this.getDatesOfTheWeek(this.currentlyViewedWeek + 1, date);
  }

  private getDatesOfTheWeek(weekNumber: number, date: Date): Date[] {
    const weekType = this.userService.config.calendar.weekStartDay === 'mon' ? 'isoWeek' : 'week';
    const startDate = moment(date).add(weekNumber, 'week').startOf(weekType);
    const endDate = moment(date).add(weekNumber, 'week').endOf(weekType);

    const now = startDate.clone(), dates: Date[] = [];

    while (now.isSameOrBefore(endDate)) {
      dates.push(now.toDate());
      now.add(1, 'days');
    }
    return dates;
  };
}
