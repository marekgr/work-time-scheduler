import {
  AfterViewChecked,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges, OnDestroy,
  Output,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { MatCalendar, MatCalendarCellClassFunction } from '@angular/material/datepicker';
import { UserService } from '../../../../services/user.service';
import { Subscription } from 'rxjs';
import Holiday from 'date-holidays'

@Component({
  selector: 'wts-calendar-full',
  templateUrl: './calendar-full.component.html',
  styleUrls: ['./calendar-full.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CalendarFullComponent implements OnChanges, AfterViewChecked, OnDestroy {

  @Input()
  selectedDate: Date | null = new Date();

  @Input()
  fullFilledDates: Date[];

  @Output()
  dateChange: EventEmitter<Date> = new EventEmitter<Date>();

  @Output()
  calendarHeight: EventEmitter<number> = new EventEmitter<number>();

  @ViewChild('calendar')
  calendar: MatCalendar<Date>;

  @ViewChild('calendar', { read: ElementRef })
  calendarHTML: ElementRef;

  currentDate = new Date();
  currentCalendarHeight: number;

  private subscription: Subscription;
  private holiday: Holiday;

  constructor(private userService: UserService) {
    this.holiday = new Holiday();
    this.holiday.init('PL');
    this.subscription = this.userService.configChanged.subscribe(() => {
      if(this.calendar) {
        this.calendar.updateTodaysDate();
      }
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.calendar && changes.fullFilledDates) {
      this.calendar.updateTodaysDate();
    }
    if (this.calendar && changes.selectedDate) {
      this.calendar._goToDateInView(changes.selectedDate.currentValue, 'month')
    }
  }

  ngAfterViewChecked() {
    if (this.calendar) {
      const height = this.calendarHTML.nativeElement.clientHeight;
      if (height !== this.currentCalendarHeight) {
        this.currentCalendarHeight = height;
        setTimeout(() => this.calendarHeight.emit(this.currentCalendarHeight), 0);
      }
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  dateClass: MatCalendarCellClassFunction<Date> = (cellDate, view) => {
    let cellClasses = '';
    if (this.isWeekend(cellDate)) {
      cellClasses += 'weekend-date-class ';
    }
    if (this.isHoliday(cellDate)) {
      cellClasses += 'holiday-date-class ';
    }
    if (this.userService.config.calendar.showFulfilledDates && view === 'month' && !this.isWeekend(cellDate)) {
      cellClasses += this.fullFilledDates
        .find(it => it.toDateString() === cellDate.toDateString())
        ? 'highlight-full'
        : (cellDate <= new Date()) ? 'highlight-empty' : '';
    }
    return cellClasses;
  };

  onDateSelected(date: any): void {
    this.selectedDate = date;
    this.dateChange.emit(date);
  }

  private isWeekend(date: Date): boolean {
    return date.getDay() === 0 || date.getDay() === 6;
  }

  private isHoliday(date: Date) {
    return this.holiday.isHoliday(date);
  }
}

