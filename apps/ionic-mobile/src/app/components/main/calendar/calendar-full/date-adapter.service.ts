import { MAT_DATE_LOCALE, NativeDateAdapter } from '@angular/material/core';
import { Platform } from '@angular/cdk/platform';
import { Inject, Injectable } from '@angular/core';
import { UserService } from '../../../../services/user.service';

@Injectable()
export class CalendarDateAdapter extends NativeDateAdapter {

  constructor(@Inject(MAT_DATE_LOCALE) matDateLocale: string, platform: Platform, private dataService: UserService) {
    super(matDateLocale, platform);
  }

  getFirstDayOfWeek(): number {
    return this.dataService.config.calendar.weekStartDay === 'mon' ? 1 : 0;
  }
}
