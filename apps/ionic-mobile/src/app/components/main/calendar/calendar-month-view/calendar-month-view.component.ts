import { Component, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import * as moment from 'moment';
import { MatCalendar, MatCalendarCellClassFunction } from '@angular/material/datepicker';

/*UNUSED COMPONENT YET. INTENDED FOR SWIPE IMPLEMENTATION*/
@Component({
  selector: 'wts-calendar-month-view',
  templateUrl: './calendar-month-view.component.html',
  styleUrls: ['./calendar-month-view.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CalendarMonthViewComponent {

  @ViewChild('ionSlides')
  ionSlides: HTMLIonSlidesElement;

  @ViewChild('previous') previousCalendar: MatCalendar<Date>;
  @ViewChild('current') currentCalendar: MatCalendar<Date>;
  @ViewChild('next') nextCalendar: MatCalendar<Date>;

  slideOpts = {
    loop: true,
    initialSlide: 1,
    runCallbacksOnInit: false,
    loopPreventsSlide: false,
    touchReleaseOnEdges: true,
    speed: 400
  };

  startAt: Date;

  today = new Date();

  selected: Date | null = this.today;

  previousMonth: Date;
  currentMonth: Date = this.today;
  nextMonth: Date;
  currentlyViewedMonth = 0;

  // temp
  fullFilledDates = [
    new Date(2022, 0, 1, 0, 0, 0),
    new Date(2022, 0, 5, 0, 0, 0)
  ];

  constructor() {
    this.updateMonthDateViews();
  }

  dateClass: MatCalendarCellClassFunction<Date> = (cellDate, view) => {
    if (cellDate <= new Date() && view === 'month' && !this.isWeekend(cellDate)) {
      return this.fullFilledDates.find(it => it.toDateString() === cellDate.toDateString()) ? 'highlight-full' : 'highlight-empty';
    }
    return '';
  };


  // For use with calendar with swipe
  getStartDateForMonthView(monthsToAdd: number): Date {
    const newDate = new Date(this.today.getUTCFullYear(), this.today.getMonth(), 1, 0, 0, 0);
    return moment(newDate).add(monthsToAdd, "M").toDate();
  }

  slideNext(): void {
    this.currentlyViewedMonth = this.currentlyViewedMonth + 1;
    this.updateMonthDateViews();
    this.refreshCalendars();
    this.ionSlides.slideTo(2, 0, false);
  }

  slidePrevious(): void {
    this.currentlyViewedMonth = this.currentlyViewedMonth - 1;
    this.updateMonthDateViews();
    this.refreshCalendars();
    this.ionSlides.slideTo(2, 0, false);
  }

  private updateMonthDateViews() {
    this.previousMonth = this.getStartDateForMonthView(this.currentlyViewedMonth - 1);
    this.currentMonth = this.getStartDateForMonthView(this.currentlyViewedMonth);
    this.nextMonth = this.getStartDateForMonthView(this.currentlyViewedMonth + 1);
    console.log('Previous: ',this.previousMonth.toDateString());
    console.log('Current: ',this.currentMonth.toDateString());
    console.log('Next: ',this.nextMonth.toDateString());
  }

  private isWeekend(date: Date): boolean {
    return date.getDay() === 5 || date.getDay() === 6;
  }

  private refreshCalendars(): void {
    this.previousCalendar._goToDateInView(this.previousMonth, 'month');
    this.currentCalendar._goToDateInView(this.currentMonth, 'month');
    this.nextCalendar._goToDateInView(this.previousMonth, 'month');
  };
}
