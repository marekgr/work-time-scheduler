import {Component, ElementRef, OnDestroy, AfterViewInit, ViewChild} from '@angular/core';
import {FetchMontRequest, ScheduleService, UpdateMonthRequest} from '../../services/schedule.service';
import * as moment from 'moment'
import {DayScheduleItem, MonthSchedule} from '../../../../../../libs/model/api';
import {SelectionService} from '../../services/selection.service';
import {AuthService} from '../../services/auth.service';
import {noop, Subscription} from 'rxjs';
import {ToastService} from '../../services/common/toast.service';

@Component({
  selector: 'wts-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements AfterViewInit, OnDestroy {

  selectedDate = new Date();

  today = new Date();

  dataReady: boolean;

  allSchedules: DayScheduleItem[] = [];

  monthScheduleModel: MonthSchedule;

  fulFilledDates: Date[];

  calendarHeight = 0;

  @ViewChild('schedulesComponent')
  schedulesComponent: ElementRef;

  subscription: Subscription[] = [];

  constructor(private scheduleService: ScheduleService,
              public selectionService: SelectionService,
              private toast: ToastService,
              private auth: AuthService) {
  }

  ngOnDestroy() {
    this.subscription.map(sub => sub.unsubscribe());
  }

  ngAfterViewInit(): void {
    this.fetchMonthData(this.today).then(noop);
  }

  onSelectedDateChange(date: Date) {
    if (!this.isSameMonthAndYear(this.selectedDate, date)) {
      this.fetchMonthData(date).then(noop);
    }
    this.selectedDate = date;
    this.allSchedules = this.monthScheduleModel.schedules;
  }

  onScheduleListChanged(schedules: DayScheduleItem[]): void {
    this.monthScheduleModel.schedules = schedules;
    this.updateMonthData();
  }

  isTodaySelected(): boolean {
    return moment(this.today).isSame(this.selectedDate, 'day')
  }

  setSelectedDayToToday(): void {
    this.selectedDate = new Date();
  }

  updateHeight(height: number) {
    this.calendarHeight = height;
  }

  updateScheduleListHeight(expanding: boolean): void {
    if (expanding) {
      this.schedulesComponent.nativeElement.style.height = '1000px';
    } else {
      this.schedulesComponent.nativeElement.style.height = `${document.body.clientHeight - this.schedulesComponent.nativeElement.getBoundingClientRect().top}px`
    }
  }

  private async fetchMonthData(date: Date) {
    const user = await this.auth.getUser();
    console.log('FetchData')
    if (user) {
      const request = new FetchMontRequest(user.uid, {month: date.getMonth(), year: date.getUTCFullYear()});
      this.subscription.push(this.scheduleService.fetchMonth(request)
        .subscribe(data => {
            this.monthScheduleModel = data
            this.dataReady = false;
            setTimeout(() => this.dataReady = true, 500);
            this.allSchedules = this.monthScheduleModel.schedules;
            this.fulFilledDates = this.monthScheduleModel?.schedules.map(value => value.date);
          },
          error => {
            console.error(error);
            this.toast.presentShort(error)
          }
        ));
    }
  }

  private updateMonthData(): void {
    this.auth.getUser().then(user => {
      if (user) {
        const request = new UpdateMonthRequest(user.uid, this.monthScheduleModel)
        this.scheduleService.updateMonth(request)
      }
    })
  }

  private isSameMonthAndYear(date1: Date, date2: Date) {
    return `${date1.getMonth()}.${date1.getUTCFullYear()}` === `${date2.getMonth()}.${date2.getUTCFullYear()}`
  }
}
