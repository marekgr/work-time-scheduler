import { Component, Inject } from '@angular/core';
import { DayScheduleItem } from '../../../../../../../../libs/model/api';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'wts-schedule-item-modify',
  templateUrl: './schedule-item-modify.component.html',
  styleUrls: ['./schedule-item-modify.component.scss']
})
export class ScheduleItemModifyComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: {schedule: DayScheduleItem}){}
}
