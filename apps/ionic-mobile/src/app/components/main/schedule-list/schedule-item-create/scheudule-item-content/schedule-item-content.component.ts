import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DayScheduleItem } from '../../../../../../../../../libs/model/api';
import * as moment from 'moment';

@Component({
  selector: 'wts-schedule-item-content',
  templateUrl: './schedule-item-content.component.html',
  styleUrls: ['./schedule-item-content.component.scss']
})
export class ScheduleItemContentComponent {

  @Input() scheduleData: DayScheduleItem;

  @Output() scheduleDataChange = new EventEmitter<DayScheduleItem>();

  minuteValues: number[] = [];

  constructor() {
    //TODO Make minute interval customizable
    for(let i = 0; i < 60; i = i + 5) {
      this.minuteValues.push(i);
    }
  }

  onChange(): void {
    this.recalculateTimeDiff();
    this.scheduleDataChange.emit(this.scheduleData);
  }

  recalculateTimeDiff(): void {
    this.scheduleData.timeDiff = moment.duration(moment(this.scheduleData.timeTo, 'HH:mm').diff(moment(this.scheduleData.timeFrom, 'HH:mm'))).asHours()
  }
}
