import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { DayScheduleItem } from '../../../../../../../../libs/model/api';
import { Platform } from '@ionic/angular';
import * as moment from 'moment';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'wts-schedule-item-create',
  templateUrl: './schedule-item-create.component.html',
  styleUrls: ['./schedule-item-create.component.scss']
})
export class ScheduleItemCreateComponent {

  @Input()
  selectedDate: Date;

  @Output()
  scheduleCreated: EventEmitter<DayScheduleItem> = new EventEmitter<DayScheduleItem>();
  schedule: DayScheduleItem;

  @ViewChild('bottomSheetModal')
  bottomSheetModal: any;
  open = false;
  modalHeight: number;

  constructor(private platform: Platform,
              private userService: UserService) {
    this.platform.keyboardDidShow.subscribe(() => {
      this.expandBottomSheet();
    });

    this.platform.keyboardDidHide.subscribe(() => {
      this.collapseBottomSheet();
    });
  }

  expandBottomSheet(): void {
    if (!this.modalHeight) {
      this.modalHeight = this.bottomSheetModal.el.firstElementChild.offsetHeight / 2;
    }
    const ionModalContent = this.bottomSheetModal.el.firstElementChild.offsetParent;
    ionModalContent.style.bottom = `${this.modalHeight}px`
  }

  collapseBottomSheet(): void {
    const ionModalContent = this.bottomSheetModal.el.firstElementChild.offsetParent;
    ionModalContent.style.bottom = `0`
  }

  recalculateTimeDiff(): void {
    const result = moment.duration(moment(this.schedule.timeTo, 'HH:mm').diff(moment(this.schedule.timeFrom, 'HH:mm'))).asHours();
    this.schedule.timeDiff = Math.round(result * 100) / 100;
  }

  submitNewSchedule(): void {
    this.schedule.date = this.selectedDate;
    this.recalculateTimeDiff();
    this.scheduleCreated.emit(this.schedule);
    this.open = false;
  }

  openBottomSheet(): void {
    this.open = true;
    this.createDefaultSchedule();
  }

  private createDefaultSchedule(): void {
    const defaultScheduleProps = this.userService.getConfig().schedules;
    this.schedule = new DayScheduleItem();
    this.schedule.date = this.selectedDate;
    this.schedule.timeFrom = defaultScheduleProps.defaultWorkStartTime;
    this.schedule.timeTo = defaultScheduleProps.defaultWorkEndTime;
    this.schedule.description = defaultScheduleProps.defaultDescription;
  }
}
