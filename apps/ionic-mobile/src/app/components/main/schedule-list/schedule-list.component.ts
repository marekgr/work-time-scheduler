import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { DayScheduleItem } from '../../../../../../../libs/model/api';
import { MatDialog } from '@angular/material/dialog';
import { ScheduleItemModifyComponent } from './schedule-item-modify/schedule-item-modify.component';
import * as moment from 'moment';
import Holidays, { HolidaysTypes } from 'date-holidays';

@Component({
  selector: 'wts-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: ['./schedule-list.component.scss']
})
export class ScheduleListComponent implements OnChanges {

  @Input()
  selectedDate: Date;

  @Input()
  ready: boolean;


  @Input()
  scheduleList: DayScheduleItem[];
  todayScheduleList: DayScheduleItem[];

  @Output()
  scheduleListChanged: EventEmitter<DayScheduleItem[]> = new EventEmitter<DayScheduleItem[]>()

  currentHoliday: boolean | HolidaysTypes.Holiday[];
  private holidays: Holidays;

  constructor(public dialog: MatDialog) {
    this.holidays = new Holidays();
    this.holidays.init('PL');
    this.updateCurrentHoliday();
  }

  private updateCurrentHoliday() {
    this.currentHoliday = this.holidays.isHoliday(this.selectedDate);
  }

  modifySchedule(schedule: DayScheduleItem) {
    this.dialog.open(ScheduleItemModifyComponent, {
      maxWidth: '85vw',
      height: '45vh',
      width: '85vw',
      data: {
        schedule: Object.assign({}, schedule)
      },
    }).afterClosed().subscribe(schedule => {
      if(schedule) {
        this.onScheduleModified(schedule)
      }
    })
  }

  onScheduleCreated(schedule: DayScheduleItem): void {
    this.scheduleList.push(schedule);
    this.scheduleListChanged.emit(this.scheduleList);
  }

  onScheduleDeleted(schedule: DayScheduleItem): void {
    this.scheduleList = this.scheduleList.filter(item => item.id !== schedule.id);
    this.scheduleListChanged.emit(this.scheduleList);
  }

  onScheduleModified(schedule: DayScheduleItem): void {
    const index = this.scheduleList.findIndex(item => item.id == schedule.id);
    this.scheduleList[index] = schedule;
    this.scheduleListChanged.emit(this.scheduleList);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.scheduleList) {
      this.todayScheduleList = this.getTodaySchedules(changes.scheduleList.currentValue as DayScheduleItem[]);
    }
    if (changes.selectedDate) {
      this.todayScheduleList = this.getTodaySchedules(this.scheduleList);
      this.updateCurrentHoliday();
    }
  }

  private getTodaySchedules(input: DayScheduleItem[]): DayScheduleItem[] {
    if (!input) {
      return [];
    }
    return input.filter(item => moment(item.date).isSame(this.selectedDate, 'day'));
  }

  trackItems(index: number, item: DayScheduleItem) {
    return item.id;
  }


}
