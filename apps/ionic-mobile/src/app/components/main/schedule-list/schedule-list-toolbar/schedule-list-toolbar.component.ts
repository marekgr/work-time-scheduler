import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { SelectionService } from '../../../../services/selection.service';
import { DayScheduleItem } from '../../../../../../../../libs/model/api';
import * as moment from 'moment';

@Component({
  selector: 'wts-schedule-list-toolbar',
  templateUrl: './schedule-list-toolbar.component.html',
  styleUrls: ['./schedule-list-toolbar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ScheduleListToolbarComponent implements OnInit {

  @Input()
  allSchedules: DayScheduleItem[];

  @Input()
  selectedDate: Date;

  @Output()
  removeSchedules: EventEmitter<DayScheduleItem[]> = new EventEmitter<DayScheduleItem[]>();

  constructor(private selectionService: SelectionService) { }

  ngOnInit(): void {
    console.log(this.allSchedules)
  }

  deselectAll(): void {
    this.selectionService.selectedScheduleList = [];
  }

  selectAll(): void {
    const selection = this.getSchedulesForDate(this.allSchedules, this.selectedDate);
    selection.forEach(it => this.selectionService.selectionChange(it, true));
  }

  delete(): void {
    const result = this.allSchedules.filter(
      it => !this.selectionService.selectedScheduleList.find(schedule => schedule.id === it.id)
    )
    this.removeSchedules.emit(result);
    this.deselectAll();
  }

  private getSchedulesForDate(input: DayScheduleItem[], selectedDate: Date): DayScheduleItem[] {
    if (!input) {
      return [];
    }
    return input.filter(item => moment(item.date).isSame(selectedDate, 'day'));
  }
}
