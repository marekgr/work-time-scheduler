import { Component, Input } from '@angular/core';

@Component({
  selector: 'wts-schedule-item-empty',
  templateUrl: './schedule-item-empty.component.html',
  styleUrls: ['./schedule-item-empty.component.scss']
})
export class ScheduleItemEmptyComponent {

  @Input()
  optionalTitle: string;

  constructor() { }
}
