import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { SelectionService } from '../../../../services/selection.service';
import { DayScheduleItem } from '../../../../../../../../libs/model/api';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'wts-schedule-item',
  templateUrl: './schedule-item.component.html',
  styleUrls: ['./schedule-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ScheduleItemComponent {

  @Input()
  schedule: DayScheduleItem;

  @Output()
  delete: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  modify: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public selectionService: SelectionService) {
  }

  checkboxChange(schedule: DayScheduleItem, event: MatCheckboxChange) {
    this.selectionService.selectionChange(schedule, event.checked)
  }

  formatTimeDiff(hourTime: number) {
    const totalMinutes = (hourTime * 60);
    const hourPart = Math.floor(totalMinutes / 60);
    const minutePart = Math.round( totalMinutes % 60);
    return `${hourPart}h ${minutePart > 0 ? (minutePart + 'm') : ''}`;
  }
}
