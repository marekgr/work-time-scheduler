import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  OnChanges,
  Output, SimpleChanges,
  ViewChild
} from '@angular/core';
import { GestureConfig, GestureController, GestureDetail } from '@ionic/angular';

@Component({
  selector: 'wts-top-expand',
  templateUrl: './top-expand.component.html',
  styleUrls: ['./top-expand.component.scss']
})
export class TopExpandComponent implements AfterViewInit, OnChanges {

  SWIPE_THRESHOLD = 0.1;

  expanded = false;

  startPosY = 0;

  swipeDistance = 0;

  @Input()
  containerHeight: number;

  @ViewChild('expandContainer')
  expandContainer: ElementRef;

  @ViewChild('dragContainer')
  dragContainer: ElementRef;

  @Output()
  expanding: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private gestureCtrl: GestureController,
              private ngZone: NgZone) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.containerHeight && this.expanded) {
      this.toggleExpand(this.expanded)
    }
  }

  ngAfterViewInit() {
    const gesture = this.gestureCtrl.create({
      el: this.dragContainer.nativeElement,
      gesturePriority: 10,
      direction: 'y',
      onStart: (detail) => this.touchStart(detail),
      onMove: (detail) => this.touchMove(detail),
      onEnd: (detail) => this.ngZone.run(() => this.touchEnd()),
    } as GestureConfig)

    gesture.enable();
  }

  toggleExpand(state: boolean | null = null): void {
    this.expanded = state != null ? state : !this.expanded;
    if (this.expanded) {
      this.expandContainer.nativeElement.style.height = this.containerHeight + 'px';
      setTimeout(() => this.expanding.emit(false), 400);
    } else {
      this.expandContainer.nativeElement.style.height = 0 + 'px';
      setTimeout(() => this.expanding.emit(false), 400);
    }
  }

  touchStart(event: GestureDetail): void {
    this.disableAnimation();
    this.startPosY = event.currentY;
    this.expanding.emit(true);
  }

  touchMove(event: GestureDetail): void {
    if (!this.expanded) {
      this.swipeDistance = event.deltaY;
      this.expandContainer.nativeElement.style.height = this.swipeDistance + 'px';
    } else {
      this.swipeDistance = this.startPosY - event.currentY;
      this.expandContainer.nativeElement.style.height = (this.containerHeight - this.swipeDistance) + 'px';
    }
  }

  touchEnd(): void {
    this.enableAnimation();
    if (this.swipeDistance > this.containerHeight * this.SWIPE_THRESHOLD) {
      this.toggleExpand();
    } else {
      this.toggleExpand(this.expanded);
    }
    this.startPosY = 0;
    this.swipeDistance = 0;
  }

  private disableAnimation(): void {
    this.expandContainer.nativeElement.style.transition = 'none';
  }

  private enableAnimation(): void {
    this.expandContainer.nativeElement.style.transition = '400ms height';
  }
}
