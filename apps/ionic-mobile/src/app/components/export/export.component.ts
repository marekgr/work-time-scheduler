import { Component, EventEmitter, ViewEncapsulation } from '@angular/core';
import { DateRange } from '@angular/material/datepicker';
import { MatSelectChange } from '@angular/material/select';
import * as moment from 'moment';
import { ExportFormat, ExportService } from './export.service';
import { IonRouterOutlet, NavController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'wts-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ExportComponent {

  format: ExportFormat = ExportFormat.xlsx;

  dateRange: 'currentMonth' | 'lastMonth' | 'custom' = 'currentMonth';

  fileName = `Schedule-Export-${moment().format('DD_MM_YYYY_HH_mm')}`;

  selectedRangeValue: DateRange<Date> | undefined;
  selectedRangeValueChange = new EventEmitter<DateRange<Date>>();

  constructor(private exportService: ExportService,
              private platform: Platform,
              private navController: NavController) {
    this.dateRangeChange({ value: 'currentMonth' } as MatSelectChange)
    this.platform.backButton.subscribeWithPriority(0, () => {
      this.navController.back();
    })
  }

  export(): void {
    if (this.selectedRangeValue) {
      this.exportService.exportData(this.selectedRangeValue, this.format, this.fileName);
    }
  }

  selectedChange(m: any) {
    if (!this.selectedRangeValue?.start || this.selectedRangeValue?.end) {
      this.selectedRangeValue = new DateRange<Date>(m, null);
    } else {
      const start = this.selectedRangeValue.start;
      const end = m;
      if (end < start) {
        this.selectedRangeValue = new DateRange<Date>(end, start);
      } else {
        this.selectedRangeValue = new DateRange<Date>(start, end);
      }
    }
    this.selectedRangeValueChange.emit(this.selectedRangeValue);
  }

  dateRangeChange(event: MatSelectChange): void {
    if (event.value === 'lastMonth') {
      this.selectedRangeValue = new DateRange<Date>(
        moment().subtract(1, 'month').startOf('month').toDate(),
        moment().subtract(1, 'month').endOf('month').toDate()
      );
    }
    if (event.value === 'currentMonth') {
      this.selectedRangeValue = new DateRange<Date>(
        moment().startOf('month').toDate(),
        moment().endOf('month').toDate()
      );
    }
    if (event.value === 'custom') {
      this.selectedRangeValue = undefined;
    }
  }

  getStartDate(): string | undefined {
    return this.selectedRangeValue?.start?.toLocaleDateString();
  }


  getEndDate(): string | undefined {
    return this.selectedRangeValue?.end?.toLocaleDateString();
  }
}
