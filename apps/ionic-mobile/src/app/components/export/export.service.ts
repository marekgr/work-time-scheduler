import { Injectable } from '@angular/core';
import { ExcelService } from '../../services/export/excel.service';
import { DateRange } from '@angular/material/datepicker';

@Injectable({
  providedIn: 'root'
})
export class ExportService {

  constructor(private excelService: ExcelService) { }

  exportData(dateRange: DateRange<Date>, format: ExportFormat, fileName: string): void {
    switch (format) {
      case ExportFormat.xlsx: this.excelService.generateXlsx(dateRange, fileName);
    }
  }
}

export enum ExportFormat {
  xlsx='xlsx',
}
