import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import firebase from 'firebase/compat';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'wts-settings-user-section',
  templateUrl: './settings-user-section.component.html',
  styleUrls: ['./settings-user-section.component.scss']
})
export class SettingsUserSectionComponent {

  userName: string;
  email: string;

  @Input()
  userData: firebase.User | null;

  @Output()
  userDataChanged: EventEmitter<firebase.User> = new EventEmitter<firebase.User>();

  constructor(private dialog: MatDialog) { }

  openDialog(templateRef: TemplateRef<any>) {
    this.refreshData();
    const dialogRef = this.dialog.open(templateRef, {
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(value => {
      if (value) {
        this.updateSettings();
      }
    })
  }

  refreshData() {
    this.userName  = this.userData?.displayName  ? this.userData.displayName : '';
    this.email  = this.userData?.email  ? this.userData.email : '';
  }

  updateSettings(): void {
    const user = JSON.parse(JSON.stringify(this.userData)) as firebase.User;
    user.displayName = this.userName;
    this.userDataChanged.emit(user);
  }
}
