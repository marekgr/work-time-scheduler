import { Component, OnInit } from '@angular/core';
import firebase from 'firebase/compat';
import { AuthService } from '../../services/auth.service';
import {UpdateUserDataRequest, UserConfig, UserService} from '../../services/user.service';

@Component({
  selector: 'wts-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  config: UserConfig = new UserConfig();

  user: firebase.User | null;

  constructor(private auth: AuthService,
              private userService: UserService) {
  }

  async ngOnInit() {
    this.config = this.userService.getConfig();
    this.user = await this.auth.getUser();
  }

  async updateSettings(): Promise<void> {
    const user = await this.auth.getUser();
    if (user) {
      const userCopy = JSON.parse(JSON.stringify(user)) as BasicUser;
      userCopy.displayName = this.user?.displayName ? this.user.displayName : '';
      const request = new UpdateUserDataRequest(userCopy.uid, userCopy.email, userCopy.displayName, this.config);
      await this.userService.updateUserData(request);
      await user.updateProfile({displayName: userCopy.displayName});
    }
  }
}

export class BasicUser {
  displayName: string;
  email: string;
  uid: string;
}
