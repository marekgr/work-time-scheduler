import { Component, EventEmitter, Input, Output } from '@angular/core';
import { GeneralSettings } from '../../../services/user.service';

@Component({
  selector: 'wts-settings-general-section',
  templateUrl: './settings-general-section.component.html',
  styleUrls: ['./settings-general-section.component.scss']
})
export class SettingsGeneralSectionComponent {

  language: string

  @Input()
  set generalSettings(settings: GeneralSettings) {
    this.language = settings.language
  }

  @Output()
  generalSettingsChanged: EventEmitter<GeneralSettings> = new EventEmitter<GeneralSettings>();

  constructor() { }

  updateSettings(): void {
    const settings = new GeneralSettings();
    settings.language = this.language;
    this.generalSettingsChanged.emit(settings);
  }
}
