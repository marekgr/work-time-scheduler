import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CalendarSettings } from '../../../services/user.service';

@Component({
  selector: 'wts-settings-calendar-section',
  templateUrl: './settings-calendar-section.component.html',
  styleUrls: ['./settings-calendar-section.component.scss']
})
export class SettingsCalendarSectionComponent {

  @Input()
  calendarSettings: CalendarSettings;

  @Output()
  calendarSettingsChanged: EventEmitter<CalendarSettings> = new EventEmitter<CalendarSettings>();

  constructor() {
  }

  updateSettings(): void {
    this.calendarSettingsChanged.emit(this.calendarSettings);
  }

}
