import { Component, EventEmitter, Input, Output, TemplateRef, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ScheduleSettings } from '../../../services/user.service';

@Component({
  selector: 'wts-settings-schedule-section',
  templateUrl: './settings-schedule-section.component.html',
  styleUrls: ['./settings-schedule-section.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SettingsScheduleSectionComponent {

  timeFrom = '07:00';
  timeTo = '15:00';
  timeInterval = 1;
  description= '';


  minuteValues: number[] = [];

  @Input()
  scheduleSettings: ScheduleSettings;

  @Output()
  scheduleSettingsChanged: EventEmitter<ScheduleSettings> = new EventEmitter<ScheduleSettings>();

  constructor(private dialog: MatDialog) {
    for(let i = 0; i < 60; i = i + 5) {
      this.minuteValues.push(i);
    }
  }

  openDialog(templateRef: TemplateRef<any>) {
    this.refreshData();
    const dialogRef = this.dialog.open(templateRef, {
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(value => {
      if (value) {
        this.updateSettings();
      }
    })
  }

  refreshData() {
    this.timeFrom  = this.scheduleSettings.defaultWorkStartTime;
    this.timeTo = this.scheduleSettings.defaultWorkEndTime;
    this.description = this.scheduleSettings.defaultDescription;
  }

  updateSettings(): void {
    const settings = new ScheduleSettings();
    settings.defaultDescription = this.description;
    settings.defaultWorkEndTime = this.timeTo;
    settings.defaultWorkStartTime = this.timeFrom;
    this.scheduleSettingsChanged.emit(settings)
  }
}
