import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { RegisterComponent } from './components/authentication/register/register.component';
import { LoginComponent } from './components/authentication/login/login.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { DateAdapter, MAT_DATE_LOCALE, MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { ConfirmSkipAlertComponent } from './components/authentication/confirm-skip-alert/confirm-skip-alert.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from '../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainComponent } from './components/main/main.component';
import { CalendarWeekViewComponent } from './components/main/calendar/calendar-week-view/calendar-week-view.component';
import { CalendarFullComponent } from './components/main/calendar/calendar-full/calendar-full.component';
import { ScheduleListComponent } from './components/main/schedule-list/schedule-list.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { TopExpandComponent } from './components/main/top-expand/top-expand.component';
import { CalendarDateAdapter } from './components/main/calendar/calendar-full/date-adapter.service';
import { CalendarMonthViewComponent } from './components/main/calendar/calendar-month-view/calendar-month-view.component';
import { HeaderComponent } from './components/main/header/header.component';
import { ScheduleItemComponent } from './components/main/schedule-list/schedule-item/schedule-item.component';
import { MatMenuModule } from '@angular/material/menu';
import { ScheduleItemCreateComponent } from './components/main/schedule-list/schedule-item-create/schedule-item-create.component';
import { NgxMatTimepickerModule } from 'ngx-mat-timepicker';
import { ScheduleItemContentComponent } from './components/main/schedule-list/schedule-item-create/scheudule-item-content/schedule-item-content.component';
import { ScheduleItemModifyComponent } from './components/main/schedule-list/schedule-item-modify/schedule-item-modify.component';
import { MatDialogModule } from '@angular/material/dialog';
import { IonicStorageModule, Storage } from '@ionic/storage-angular';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { ScheduleListToolbarComponent } from './components/main/schedule-list/schedule-list-toolbar/schedule-list-toolbar.component';
import { SideMenuComponent } from './components/main/side-menu/side-menu.component';
import { ExportComponent } from './components/export/export.component';
import { MatSelectModule } from '@angular/material/select';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import '@angular/common/locales/global/pl';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ScheduleItemEmptyComponent } from './components/main/schedule-list/schedule-item-empty/schedule-item-empty.component';
import { AboutComponent } from './components/about/about.component';
import { SettingsComponent } from './components/settings/settings.component';
import { SettingsGeneralSectionComponent } from './components/settings/settings-general-section/settings-general-section.component';
import { SettingsCalendarSectionComponent } from './components/settings/settings-calendar-section/settings-calendar-section.component';
import { SettingsScheduleSectionComponent } from './components/settings/settings-schedule-section/settings-schedule-section.component';
import { LocalizedDatePipePipe } from './components/main/calendar/calendar-week-view/localized-date-pipe.pipe';
import { RegisterConfirmEmailComponent } from './components/authentication/register/register-confirm-email/register-confirm-email.component';
import { SettingsUserSectionComponent } from './components/settings/settings-user-section/settings-user-section.component';
import { LoginForgotPasswordComponent } from './components/authentication/login/login-forgot-password/login-forgot-password.component';
// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    ConfirmSkipAlertComponent,
    MainComponent,
    CalendarWeekViewComponent,
    CalendarFullComponent,
    ScheduleListComponent,
    TopExpandComponent,
    CalendarMonthViewComponent,
    HeaderComponent,
    ScheduleItemComponent,
    ScheduleItemCreateComponent,
    ScheduleItemContentComponent,
    ScheduleItemModifyComponent,
    ScheduleListToolbarComponent,
    SideMenuComponent,
    ExportComponent,
    ScheduleItemEmptyComponent,
    AboutComponent,
    SettingsComponent,
    SettingsGeneralSectionComponent,
    SettingsCalendarSectionComponent,
    SettingsScheduleSectionComponent,
    LocalizedDatePipePipe,
    RegisterConfirmEmailComponent,
    SettingsUserSectionComponent,
    LoginForgotPasswordComponent,
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    TranslateModule.forRoot({
      defaultLanguage: 'pl',
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    AppRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    HttpClientModule,
    MatRippleModule,
    FormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTabsModule,
    MatCardModule,
    MatMenuModule,
    NgxMatTimepickerModule,
    MatDialogModule,
    MatSelectModule,
    MatCheckboxModule,
    ReactiveFormsModule,
  ],
  providers: [
    Storage,
    FileOpener,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: MAT_DATE_LOCALE, useValue: 'pl' },
    { provide: LOCALE_ID, useValue: "pl-PL" },
    { provide: DateAdapter, useClass: CalendarDateAdapter }],
  bootstrap: [AppComponent],
})
export class AppModule {
}
