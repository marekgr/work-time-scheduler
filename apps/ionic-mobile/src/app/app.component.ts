import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage-angular';
import {FetchUserDataRequest, UserService} from './services/user.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { first } from 'rxjs';
import { DateAdapter } from '@angular/material/core';
import Holidays from 'date-holidays';

@Component({
  selector: 'wts-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  holidays: Holidays;

  constructor(private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer,
              private translate: TranslateService,
              private storage: Storage,
              private angularFireAuth: AngularFireAuth,
              private userService: UserService,
              private dateAdapter: DateAdapter<any>) {
    this.getUserSettings();
    this.appInit();
    this.holidays = new Holidays();
    this.holidays.init('PL');
    console.log(this.holidays.getHolidays(new Date(2022, 5, 1)));
  }

  private getUserSettings(): void {
    this.angularFireAuth.authState.subscribe(user => {
      if (user) {
        this.userService.fetchUserConfig(new FetchUserDataRequest(user.uid))
          .pipe(first())
          .subscribe(data => this.userService.setConfig(data));
      }
    })
  }

  private appInit(): void {
    this.matIconRegistry.addSvgIcon("googleLogo", this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icon/google.svg'));
    this.matIconRegistry.addSvgIcon("facebookLogo", this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icon/facebook.svg'));
    this.matIconRegistry.addSvgIcon("mailVerify", this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icon/email-verification.svg'));
    this.matIconRegistry.addSvgIcon("remindPassword", this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icon/password-reset.svg'));
    this.translate.setDefaultLang('pl');
    this.translate.use('pl');

    this.userService.configChanged.subscribe(config => {
      this.translate.use(config.general.language);
      this.dateAdapter.setLocale(config.general.language);
    })
  }
}
